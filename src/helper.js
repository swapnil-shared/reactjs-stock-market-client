// Stock listing endpoint
const externalEndponts = {
  getLiveStocks: {
    uri: "http://localhost:8080/stock/live",
    eventName: "stock-changed",
  },
};

export { externalEndponts };
