import React, { Component } from "react";
import "./App.css";
import StockListComponent from "./components/stock/stockListComponent";

class App extends Component {
  state = {};
  render() {
    return <StockListComponent />;
  }
}

export default App;
