import React from "react";

const StockView = (props) => {
  const trend =
    props.stock.trend === "UP" ? (
      <p className="up">&#9650;</p>
    ) : props.stock.trend === "DOWN" ? (
      <p className="down">&#9660;</p>
    ) : props.stock.trend === "INITIAL" ? (
      <p className="initial">Just launched..</p>
    ) : (
      <p className="no-change">No change..</p>
    );
  return (
    <tr>
      <td>{props.stock.symbol}</td>
      <td className="num">{props.stock.marketPrice.toFixed(2) || 0.0}</td>
      <td>{trend}</td>
    </tr>
  );
};

export default StockView;
