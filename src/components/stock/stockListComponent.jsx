import React, { Component } from "react";
import { externalEndponts } from "../../helper";
import "./stock.css";
import StockListView from "./stockListView";

class StockListComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ping: new Date(),
      stocks: [],
    };

    // check if the realtime connection is dead, reload if dead
    setInterval(() => {
      let now = new Date().getTime();
      let diff = (now - this.state.ping.getTime()) / 1000;

      // If not heard from the server in 25 secs?
      if (diff > 20) {
        // refresh the client
        window.location.reload();
      }
    }, 10000);
  }

  linkToStockServer() {
    let eventSource = new EventSource(externalEndponts.getLiveStocks.uri);
    eventSource.addEventListener(
      externalEndponts.getLiveStocks.eventName,
      function (e) {
        var updatedStocks = JSON.parse(e.data);
        this.setState((previousState) => {
          return { ping: new Date(), stocks: updatedStocks };
        });
      }.bind(this),
      false
    );
  }

  componentDidMount() {
    // get data from server after component mount
    this.linkToStockServer();
  }

  render() {
    return <StockListView stocks={this.state.stocks} />;
  }
}

export default StockListComponent;
