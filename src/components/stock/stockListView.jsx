import React from "react";
import StockView from "./stockView";

const StockListView = (props) => {
  const stockList = props.stocks.map((stock) => {
    return <StockView key={stock.symbol} stock={stock} />;
  });

  return (
    <div className="tickers">
      <table className="table">
        <thead>
          <tr>
            <th>Symbol</th>
            <th className="num">Price</th>
            <th className="num">Trend</th>
          </tr>
        </thead>
        <tbody>{stockList}</tbody>
      </table>
    </div>
  );
};

export default StockListView;
